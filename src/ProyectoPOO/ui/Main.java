package ProyectoPOO.ui;

import ProyectoPOO.bl.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static ArrayList<Usuario> usuarios = new ArrayList<>();
    static ArrayList<Retos> retos = new ArrayList<>();
    static String correoActual;
    static String claveActual;
    static String rol = "";
    public static void main(String[] args) throws IOException {

        int opcion;
        do {
            opcion = pintarMenu();
            switch (opcion) {
                case 1:
                    agregarUsuario();
                    break;
                case 2:
                    verAtletas();
                    break;
                case 3:
                    verAdmins();
                    break;
                case 4:
                    iniciarSesion();
                    break;
                case 5:
                    verSessionActual();
                    break;

            }
        } while (opcion != 0);
    }

    public static int pintarMenu() throws IOException {
        out.println("--- MENU ---");
        out.println("1. Agregar Usuario");
        out.println("2. Ver Atletas");
        out.println("3. Ver Admins");
        out.println("4. Iniciar Sesion");
        out.println("5. ver Sesion actual");
        out.println("0. Salir");
        int opcion = Integer.parseInt(in.readLine());
        return opcion;
    }

    public static void agregarUsuario() throws IOException {
        out.print("Ingrese el correo: ");
        String correo = in.readLine();
        out.print("Ingrese la clave: ");
        String clave = in.readLine();
        int opcion;
        opcion = pintarMenu2();
        switch (opcion) {
            case 1:
                out.print("Ingrese el nombre: ");
                String nombre = in.readLine();
                out.print("Ingrese el apellido: ");
                String apellido = in.readLine();
                out.print("Ingrese identificacion: ");
                String identificacion = in.readLine();
                DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                out.print("Ingrese fecha nacimiento dia/mes/año: ");
                LocalDate fechaNacimiento = LocalDate.parse(in.readLine(), fmt);
                out.print("Ingrese genero: ");
                String genero = in.readLine();
                Direccion direccion = null;
                MetodoPago metodoPago = null;
                out.println("Desea agregar un metodo de pago?");
                out.println("1. Si");
                out.println("2. No");
                int opcion2 = Integer.parseInt(in.readLine());
                if (opcion2 == 1) {
                    metodoPago = agregarTarjeta();
                }
                boolean esVerificado = false;
                int edad = calcularEdad(fechaNacimiento);
                Atleta atleta = new Atleta(clave, correo, nombre, apellido, identificacion, fechaNacimiento, genero, direccion, metodoPago, esVerificado, edad,rol);
                boolean existe = false;
                for (int i = 0; i < usuarios.size(); i++) {
                    if (usuarios.get(i).equals(atleta)) {
                        existe = true;
                        out.println("El correo ya existe, usuario no registrado");
                    }
                }
                if (!existe) {
                    usuarios.add(atleta);
                    out.println("Atleta Agregado con Exito!");
                }
                break;
            case 2:
                out.print("Ingrese el nombre: ");
                String nombreAdmin = in.readLine();
                out.print("Ingrese el apellido: ");
                String apellidoAdmin = in.readLine();
                out.print("Ingrese identificacion: ");
                String identificacionAdmin = in.readLine();
                Direccion direccionAdmin = null;
                Admin admin = new Admin(clave, correo, nombreAdmin, apellidoAdmin, identificacionAdmin, direccionAdmin,rol);
                boolean existe2 = false;
                for (int i = 0; i < usuarios.size(); i++) {
                    if (usuarios.get(i).equals(admin)) {
                        existe2 = true;
                        out.println("El correo ya existe, usuario no registrado");
                    }
                }
                if (!existe2) {
                    usuarios.add(admin);
                    out.println("Admin Agregado con Exito!");
                }
                break;
        }
    }

    public static MetodoPago agregarTarjeta() throws IOException {
        MetodoPago metodoPago = null;
        out.println("Digite el numero de tarjeta: ");
        String numeroTarjeta = in.readLine();
        out.println("Digite la marca de la tarjeta");
        out.println("1. Visa");
        out.println("2. MasterCard");
        int opcion = Integer.parseInt(in.readLine());
        String marca = "";
        if (opcion == 1) {
            marca = "Visa";
        } else if (opcion == 2) {
            marca = "Master Card";
        }
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        out.print("Ingrese fecha de vencimiento año/mes/dia: ");
        LocalDate fechaVencimiento = LocalDate.parse(in.readLine(), fmt);
        LocalDate fechaActual = LocalDate.now();
        if (fechaVencimiento.isBefore(fechaActual)) {
            out.println("Tarjeta ya expirada");
        } else {
            out.println("Ingrese el cvv: ");
            int cvv = Integer.parseInt(in.readLine());
            metodoPago = new MetodoPago(numeroTarjeta, marca, fechaVencimiento, cvv);

        }
        return metodoPago;
    }

    public static int pintarMenu2() throws IOException {
        out.println("El usuario es: ");
        out.println("1.Atleta");
        out.println("2.Admin");


        int opcion = Integer.parseInt(in.readLine());
        if(opcion ==1)
        {
            rol= "Atleta";
        }else if(opcion== 2) {
            rol = "Admin";
        }
        return opcion;
    }

    public static int calcularEdad(LocalDate fechaNacimiento) {
        LocalDate fechaNac = fechaNacimiento;
        LocalDate ahora = LocalDate.now();
        Period periodo = Period.between(fechaNac, ahora);
        int edad = periodo.getYears();
        return edad;
    }

    public static void verAtletas() {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i) instanceof Atleta) {
                Atleta atleta = (Atleta) usuarios.get(i);
                System.out.print(atleta.toString());
            }
        }
    }

    public static void verAdmins() {
        for (int i = 0; i < usuarios.size(); i++) {
            if (usuarios.get(i) instanceof Admin) {
                Admin admin = (Admin) usuarios.get(i);
                System.out.print(admin.toString());
            }
        }
    }

    public static void iniciarSesion() throws IOException {

        boolean noExiste = false;
        do{
            out.println("Ingrese el correo");
            correoActual = in.readLine();
            out.println("Ingrese la clave");
            claveActual = in.readLine();
            for (int i = 0; i < usuarios.size(); i++) {
                Usuario usuario = usuarios.get(i);
                if (correoActual.equals(usuario.getCorreo()) && claveActual.equals(usuario.getClave())) {

                    if (usuario instanceof Atleta) {
                        Atleta atleta = (Atleta) usuarios.get(i);
                        out.println("Bienvenido " + atleta.getNombre());
                        out.println("Bienvenido " + atleta.getNombre());
                        out.println("Eres rol: " + atleta.getRol());
                        out.println("Que dejas hacer?");
                        int opcion2 = 0;
                        do {
                            out.println("1.Registrarse a un reto");
                            out.println("2.Ver actividades fisicas");
                            out.println("4.Cerrar Sesion");
                            opcion2= Integer.parseInt(in.readLine());
                            if(opcion2 ==4)
                            {
                                cerrarsesion();
                            }
                        }while(opcion2 !=4);
                    } else if (usuario instanceof  Admin)
                    {
                        Admin admin = (Admin) usuarios.get(i);
                        out.println("Bienvenido " + admin.getNombre());
                        out.println("Eres rol: " + admin.getRol());
                        out.println("Que dejas hacer?");
                        int opcion = 0;
                        do {
                            out.println("1.Agregar un reto");
                            out.println("2.Agregar actividad fisica");
                            out.println("4.Cerrar Sesion");

                            opcion= Integer.parseInt(in.readLine());
                            if(opcion ==4)
                            {
                                cerrarsesion();
                            }
                        }while(opcion !=4);
                    }
                    noExiste = true;
                } else {
                    out.println("Usuario incorrecto");
                }
            }
        }
        while(!noExiste);
    }
    public static void agregarRetos()
    {
        out.println("");
    }
    public static void cerrarsesion() throws IOException
    {
        correoActual =null;
        claveActual = null;
        out.println("Hasta pronto");
    }
    public static void verSessionActual() throws IOException
    {
        out.println(correoActual);
        out.println(claveActual);
    }
}
