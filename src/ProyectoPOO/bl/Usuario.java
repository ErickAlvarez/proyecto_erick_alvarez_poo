package ProyectoPOO.bl;

import java.util.Objects;

public class Usuario {
    private String clave;
    private String correo;
    public Usuario()
    {

    }
    public Usuario(String clave, String correo) {
     setClave(clave);
     setCorreo(correo);
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return
                "Clave: "+ clave
                + " Correo: "+ correo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return Objects.equals(getCorreo(), usuario.getCorreo());
    }
}
