package ProyectoPOO.bl;

public class Admin extends Usuario {
    private String nombre;
    private String apellidos;
    private String identificacion;
    private Direccion direccion;
    private String rol;
    public Admin()
    {
        super();
        setNombre("Sin nombre");
        setApellidos("Sin apellidos");
        setIdentificacion("sin identificacion");
        setDireccion(null);
        setRol("Sin rol");
    }
    public Admin(String clave, String correo,String nombre, String apellidos, String identificacion, Direccion direccion,String rol) {
        super(clave,correo);
        setNombre(nombre);
        setApellidos(apellidos);
        setIdentificacion(identificacion);
        setDireccion(direccion);
        setRol(rol);

    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getRol() {
        return rol;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", direccion=" + direccion +
                '}'+super.toString();
    }
}
