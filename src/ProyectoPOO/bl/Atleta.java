package ProyectoPOO.bl;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Atleta extends Usuario{

    private String nombre;
    private String apellidos;
    private String identificacion;
    private LocalDate fechaNacimiento;
    private String genero;
    private Direccion direccion;
    private MetodoPago metodoPago;
    private boolean esVerificado;
    private int edad;
    private String rol;

    public Atleta(String clave, String correo, String nombre, String apellidos, String identificacion, LocalDate fechaNacimiento, String genero, Direccion direccion, MetodoPago metodoPago, boolean esVerificado,int edad,String rol) {
        super(clave, correo);
        setNombre(nombre);
        setApellidos(apellidos);
        setIdentificacion(identificacion);
        setFechaNacimiento(fechaNacimiento);
        setGenero(genero);
        setDireccion(direccion);
        setMetodoPago(metodoPago);
        setEsVerificado(esVerificado);
        setEdad(edad);
        setRol(rol);
    }
    public String getRol()
    {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Atleta() {
        super();
        setNombre("Sin nombre");
        setApellidos("Sin apellidos");
        setIdentificacion("Sin identificacion");
        setGenero("Sin genero");
        setDireccion(null);
        setMetodoPago(null);
        setEsVerificado(false);
    }

    public int getEdad(){return edad;}
    public void setEdad(int edad ){this.edad = edad;}
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public MetodoPago getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(MetodoPago metodoPago) {
        this.metodoPago = metodoPago;
    }

    public boolean isEsVerificado() {
        return esVerificado;
    }

    public void setEsVerificado(boolean esVerificado) {
        this.esVerificado = esVerificado;
    }


    public int calcularEdad(LocalDate fechaNacimiento)
    {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fechaNac = fechaNacimiento;
        LocalDate ahora = LocalDate.now();
        Period periodo = Period.between(fechaNac, ahora);
        int edad = periodo.getYears();
        return edad;
    }
    @Override
    public String toString() {
        return "Atleta{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", genero='" + genero + '\'' +
                ", direccion=" + direccion +
                ", metodoPago=" + metodoPago +
                ", esVerificado=" + esVerificado +
                ", edad=" +edad+
                '}'+super.toString();
    }
}
