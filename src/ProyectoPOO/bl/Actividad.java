package ProyectoPOO.bl;

public class Actividad {
    private int codigoActividad;
    private String nombreActividad;
    private String icono;

    public Actividad(int codigoActividad, String nombreActividad, String icono) {
      setCodigoActividad(codigoActividad);
      setNombreActividad(nombreActividad);
      setIcono(icono);
    }
    public Actividad()
    {
        setCodigoActividad(0);
        setNombreActividad("Sin nombre");
        setIcono("Sin icono");
    }

    public int getCodigoActividad() {
        return codigoActividad;
    }

    public void setCodigoActividad(int codigoActividad) {
        this.codigoActividad = codigoActividad;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }
}
