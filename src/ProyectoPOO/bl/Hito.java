package ProyectoPOO.bl;

public class Hito {
    private String nombreHito;
    private int kmActual;
    private String descripcion;
    private int idHito;

    public Hito(String nombreHito, int kmActual, String descripcion, int idHito) {
        setNombreHito(nombreHito);
        setKmActual(kmActual);
        setDescripcion(descripcion);
        setIdHito(idHito);
    }

    public String getNombreHito() {
        return nombreHito;
    }

    public void setNombreHito(String nombreHito) {
        this.nombreHito = nombreHito;
    }

    public int getKmActual() {
        return kmActual;
    }

    public void setKmActual(int kmActual) {
        this.kmActual = kmActual;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdHito() {
        return idHito;
    }

    public void setIdHito(int idHito) {
        this.idHito = idHito;
    }
}
