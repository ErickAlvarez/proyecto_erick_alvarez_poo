package ProyectoPOO.bl;

public class Retos {
    private String tipoGrupo;
    private Atleta atleta;
    private int idReto;
    private String nombreReto;
    private String descripcion;
    private int cantidadKM;
    private String medalla;
    private double puntoInicio;
    private double puntoFinal;
    private Hito hitos;
    private double costo;
    private Actividad actividad;

    public Retos(String tipoGrupo, Atleta atleta, int idReto, String nombreReto, String descripcion, int cantidadKM, String medalla, double puntoInicio, double puntoFinal, Hito hitos, double costo, Actividad actividad) {
        setTipoGrupo(tipoGrupo);
        setAtleta(atleta);
        setIdReto(idReto);
        setNombreReto(nombreReto);
        setDescripcion(descripcion);
        setCantidadKM(cantidadKM);
        setMedalla(medalla);
        setPuntoInicio(puntoInicio);
        setPuntoFinal(puntoFinal);
        setHitos(hitos);
        setCosto(costo);
        setActividad(actividad);
    }
    public Retos()
    {
        setTipoGrupo("Sin tipoGrupo");
        setAtleta(null);
        setIdReto(0);
        setNombreReto("sin nombreReto");
        setDescripcion("sin descripcion");
        setCantidadKM(0);
        setMedalla("sin medalla");
        setPuntoInicio(0.0);
        setPuntoFinal(0.0);
        setHitos(null);
        setCosto(0.0);
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    public String getTipoGrupo() {
        return tipoGrupo;
    }

    public void setTipoGrupo(String tipoGrupo) {
        this.tipoGrupo = tipoGrupo;
    }

    public Atleta getAtleta() {
        return atleta;
    }

    public void setAtleta(Atleta atleta) {
        this.atleta = atleta;
    }

    public int getIdReto() {
        return idReto;
    }

    public void setIdReto(int idReto) {
        this.idReto = idReto;
    }

    public String getNombreReto() {
        return nombreReto;
    }

    public void setNombreReto(String nombreReto) {
        this.nombreReto = nombreReto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidadKM() {
        return cantidadKM;
    }

    public void setCantidadKM(int cantidadKM) {
        this.cantidadKM = cantidadKM;
    }

    public String getMedalla() {
        return medalla;
    }

    public void setMedalla(String medalla) {
        this.medalla = medalla;
    }

    public double getPuntoInicio() {
        return puntoInicio;
    }

    public void setPuntoInicio(double puntoInicio) {
        this.puntoInicio = puntoInicio;
    }

    public double getPuntoFinal() {
        return puntoFinal;
    }

    public void setPuntoFinal(double puntoFinal) {
        this.puntoFinal = puntoFinal;
    }

    public Hito getHitos() {
        return hitos;
    }

    public void setHitos(Hito hitos) {
        this.hitos = hitos;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    @Override
    public String toString() {
        return "Retos{" +
                "tipoGrupo='" + tipoGrupo + '\'' +
                ", atleta=" + atleta +
                ", idReto=" + idReto +
                ", nombreReto='" + nombreReto + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", cantidadKM=" + cantidadKM +
                ", medalla='" + medalla + '\'' +
                ", puntoInicio=" + puntoInicio +
                ", puntoFinal=" + puntoFinal +
                ", hitos=" + hitos +
                ", costo=" + costo +
                '}';
    }
}
