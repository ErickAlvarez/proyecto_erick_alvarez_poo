package ProyectoPOO.bl;

import java.time.LocalDate;

public class MetodoPago {
    private String numTarjeta;
    private String marcaTarjeta;
    private LocalDate fechaExpira;
    private int cvv;

    public MetodoPago(String numTarjeta, String marcaTarjeta, LocalDate fechaExpira, int cvv) {
        setNumTarjeta(numTarjeta);
        setMarcaTarjeta(marcaTarjeta);
        setFechaExpira(fechaExpira);
        setCvv(cvv);
    }

    public String getNumTarjeta() {
        return numTarjeta;
    }

    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public String getMarcaTarjeta() {
        return marcaTarjeta;
    }

    public void setMarcaTarjeta(String marcaTarjeta) {
        this.marcaTarjeta = marcaTarjeta;
    }

    public LocalDate getFechaExpira() {
        return fechaExpira;
    }

    public void setFechaExpira(LocalDate fechaExpira) {
        this.fechaExpira = fechaExpira;
    }

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    @Override
    public String toString() {
        return "MetodoPago{" +
                "numTarjeta=" + numTarjeta +
                ", marcaTarjeta='" + marcaTarjeta + '\'' +
                ", fechaExpira=" + fechaExpira +
                ", cvv=" + cvv +
                '}';
    }
}
